from pypresence import Presence # The simple rich presence client in pypresence
import time
import psutil
from win32gui import GetWindowText, GetForegroundWindow

#
#time.sleep(240)
client_id = '727648999879737466'  # Put your Client ID here, this is a fake ID
RPC = Presence(client_id)  # Initialize the Presence class
RPC.connect()  # Start the handshake loop
time.perf_counter()
#Creation of the timer
start_time=time.time()

while True:  # The presence will stay on as long as the program is running

    
    continuer = True

    #Add of the performance menue in the menu
    cpu = psutil.cpu_percent()
    ram = psutil.virtual_memory().percent


    #Check of the app and update of the status
    name = GetWindowText(GetForegroundWindow())
    liste = ["Mozilla","Discord","cmd"] # Liste of the application you don't want
    for elem in liste:
        if elem in name:
            continuer = False
            if "Mozilla" in name:
                RPC.update(details="Running: Mozilla Firefox", state= "CPU : "+str(cpu)+"%    RAM : "+str(ram)+"%", large_image="main",start = start_time) 
            elif "cmd" in name:
                RPC.update(details="Running: Invite de commande",state= "CPU : "+str(cpu)+"%    RAM : "+str(ram)+"%", large_image="main",start = start_time)
            elif "Discord" in name:
                RPC.update(details="Running: Discord",state= "CPU : "+str(cpu)+"%    RAM : "+str(ram)+"%", large_image="main",start = start_time)
            elif "Connexion" in name:
                RPC.update(details="Running: Bureau à distance",state= "CPU : "+str(cpu)+"%    RAM : "+str(ram)+"%", large_image="main",start = start_time)
                 
    
    #Basic update of the status
    if continuer and name !="":
        RPC.update(details="Running: " +name,state= "CPU : "+str(cpu)+"%    RAM : "+str(ram)+"%" , large_image="main",start = start_time)
    elif name == "":
        RPC.update(details="Running: Nothing",state= "CPU : "+str(cpu)+"%    RAM : "+str(ram)+"%", large_image="main",start = start_time)

    time.sleep(15) #Wait a little bit
